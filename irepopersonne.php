<?php 
require_once "personne.php";

interface  irepoPersonne 
{
    function ajouterPersonne(Personne $personne) : void;
    function modifierPersonne($id,Personne $personne) : void;
    function supprimerPersonne($id) : void;
    function rechercherPersonne($id) : Personne;
    function getTousPersonne() : array;

}