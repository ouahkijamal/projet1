<?php
    require_once 'repopersonne.php';
    require_once 'personne.php';
    $dsn="mysql:host=localhost;dbname=gestion_personne";
    $username="root";
    $password="dev202";
    $options=[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
    define("CONNECTION",new PDO($dsn,$username,$password,$options));
    class repoPersonne implements irepoPersonne  {
        function __construct(){

        }
        function ajouterPersonne(Personne $personne):void{
            try {
                $statement = CONNECTION -> prepare(
                    "INSERT INTO personne(id,nom,prenom,age) VALUES
                    (:id,:nom,:prenom,:age);"
                );
                $statement -> execute([
                    ':id' => $personne -> get_id(),
                    ':nom' => $personne -> get_nom(),
                    ':prenom' => $personne -> get_prenom(),
                    ':age' => $personne -> get_age()
                ]);
            } catch (PDOException $e) {
                echo "Error: " . $e -> getMessage();
            }
        }
        function modifierPersonne($id,Personne $personne) : void{
            try {
                $statement = CONNECTION -> prepare(
                "UPDATE personne SET
                    nom=:nom,
                    prenom=:prenom,
                    age=:age
                    WHERE id = :id ;
                "
                );
                $statement -> execute([
                    ':id' => $id ,
                    ':nom' => $personne -> get_nom(),
                    ':prenom' => $personne -> get_prenom(),
                    ':age' => $personne -> get_age()
                ]);
            } catch (PDOException $e) {
                echo "Error: " . $e -> getMessage();
            }
        }
        function supprimerPersonne($id) : void{
            try {
                $statement = CONNECTION -> prepare(
                    "DELETE FROM personne WHERE id=:id;"
                );
                $statement -> execute([
                    ':id' => $id
                ]);
            } catch (PDOException $e) {
                echo "Error: " . $e -> getMessage();
            }
        }
        function rechercherPersonne($id): Personne {
            try {
                $statement = CONNECTION -> prepare(
                    "SELECT * FROM personne 
                    WHERE id=:id"
                );
                $statement -> execute([
                    ':id' => $id
                ]);
                $lst=$statement -> fetch(PDO::FETCH_ASSOC);
                return new Personne($lst["id"],$lst["nom"],$lst["prenom"],$lst["age"]);
            } catch (PDOException $e) {
                echo "Error: " . $e -> getMessage();
            }
        }
        function getTousPersonne() : array{
            try {
                $statement = CONNECTION -> prepare(
                    "SELECT * FROM personne;"
                );
                $statement -> execute();
                return $statement -> fetch(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                echo "Error: " . $e -> getMessage();
            }
        }
    }

?>
