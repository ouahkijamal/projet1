<?php 
class Personne {

     public $id;
     public $nom;
     public $prenom;
     public $age;

     public function __construct($id,$nom,$prenom,$age)
     {
          $this->id =$id;
          $this->nom = $nom;
          $this->prenom = $prenom;  
          $this->age = $age;
     }

     public function get_id(){
          return $this ->id;
     }
     public function get_nom(){
          return $this -> nom;
     }
     public function get_prenom(){
          return $this ->prenom;
     }
     public function get_age(){
          return $this ->age;
     }
     
     public function set_id($id){
          $this->id = $id;
     }

     public function set_designation($nom){
          $this-> nom= $nom;
     }
     public function set_prenom($prenom){
          $this->prenom = $prenom;
     }
     public function set_age($age){
          $this->age = $age;
     }



}
?>


